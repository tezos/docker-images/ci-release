FROM alpine:3.16

SHELL ["/bin/sh", "-eu", "-c"]

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

# hadolint ignore=DL3018,DL3019
RUN apk upgrade \
 && apk add \
    bash \
    bzip2 \
    ca-certificates \
    curl \
    git \
    gnupg \
    jq \
    make \
    openssh-client \
    unzip \
    wget \
    xz \
    zip \
    python3 \
    libc6-compat \
 && rm -rf /var/cache/apk/*

WORKDIR /tmp

# Set by Docker buildx
ARG TARGETARCH

# GitLab Release command-line tool
# https://gitlab.com/gitlab-org/release-cli/-/releases
RUN curl -fsSL "https://gitlab.com/gitlab-org/release-cli/-/releases/v0.11.0/downloads/bin/release-cli-linux-${TARGETARCH}" \
           -o /usr/local/bin/release-cli \
 && chmod 755 /usr/local/bin/release-cli

WORKDIR /

# Download and install the Google Cloud SDK
ENV GCLOUD_VERSION=472.0.0
RUN curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz && \
    tar xzf google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz && \
    rm google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz

ENV PATH /google-cloud-sdk/bin:$PATH

# Initialize the Google Cloud SDK
RUN gcloud components install core gsutil && \
    gcloud components install kubectl