# CI - Release

Docker multi-arch image meant to be used in pipeline jobs to create Gitlab generic packages and releases.

https://gitlab.com/tezos/tezos

* Archives (bzip2, xz, zip)
* GitLab release CLI
* Git `>= 2.35`

Mirrored from public GitLab container registry to public GCP Artifact Registry to be closer to GitLab runners/executors.
